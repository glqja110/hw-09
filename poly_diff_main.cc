#include "poly_diff.h"

using namespace std; 

int main() {
  Calculus cal;
  string all_letters;
  int put_number;  //변수에 넣을 숫자를 의미하는 변수
  while(true) {
    cin>>all_letters;
    if(all_letters=="quit") break;

    cal.examine(all_letters); //한줄로 다 적은 것을 분석하는 함수

    cin>>put_number;
    cout<<cal.Result(put_number)<<endl;  //결과를 출력
  }
  return 0;	
}
