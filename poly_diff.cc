#include "poly_diff.h"

using namespace std; 

void Calculus::examine(string all_letters) {
  //vector<int> coefficient;  //계수 모아놓는 벡터
  //vector<int> upper_number;  //제곱수 모아놓는 벡터

  int minus=0; //음수인지 아닌지 확인
  int upper=0; //제곱수인지 확인
  int number=-1;
  for(int i=0;i<all_letters.length();++i) {
    char c=all_letters[i];
    if(isdigit(c)) { //c 부분이 숫자면
      int num = (int)c - (int)'0';  //문자로 입력된 것들 숫자로 바꾸는 작업.
      if(number < 0) number = num; 
      else number = number * 10 + num;  //12 나 123 처럼 한자리 이상으로 넘어갈때를 작업 해주는 과정.

      if(i==all_letters.length()-1&&upper==0) { //그냥 계수만 있고 끝나면 (상수일 때) 제곱수에 0을 집어넣는다
        upper_number.push_back(0); 
        coefficient.push_back(number);
      }
      else if(i==all_letters.length()-1&&upper==1) { //그냥 계수만 있고 끝나면 (상수일 때) 제곱수에 0을 집어넣는다
        upper_number.push_back(number); 
      }
    } 
    else if (isalpha(c)) {
      if(minus==1&&number!=-1) number=(-1)*number;  //계수가 음수이면
      if(minus==0&&number==-1) number=1;
      if(i!=all_letters.length()-1) { //끝이 x로 안 끝나는 경우)
        coefficient.push_back(number);
        number=-1;
        minus=0;
      }
      else {
        coefficient.push_back(number);
        upper_number.push_back(1);
      }
    }
    else if (c=='^') {
      upper=1;
    }
    else if (c=='-') {
      minus=1;
    }
    else if (c=='+') {
      if(upper==1) {
        if(minus==1) number=(-1)*number;
        upper_number.push_back(number);
        upper=0;
        number=-1;
      }
      else {
	upper_number.push_back(1);
	number=-1;
      }
    }
  }
}

int Calculus::Result(int put_number) {
  int result=0;
  for(int i=0;i<coefficient.size();++i) {
    if(upper_number[i]==0) result+=0;
    else if (upper_number[i]==1) result+=coefficient[i];
    else {
      int num=1;
      for(int j=0;j<upper_number[i]-1;++j) {
        num*=put_number;
      }
      result+=coefficient[i]*upper_number[i]*num;
    }
  }
  return result;
  coefficient.clear();
  upper_number.clear();
}
