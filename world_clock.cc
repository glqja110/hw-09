using namespace std;
#include "world_clock.h"

WorldClock::WorldClock() {
  all_count=0;
  time_difference_=0;

  string STRING;
  ifstream infile;
  infile.open ("timezone.txt");
  string sLine = "";
  while (!infile.eof())
  {
    getline(infile, sLine);
    if(sLine!="") file_strings.push_back(sLine);
  }
  infile.close();
  string tmps;
  for(int i=0;i<file_strings.size();++i) {
    tmps=file_strings[i];
    string name;
    int number=-1;
    for(int j=0;j<tmps.length();++j) {
      char c=tmps[j];
      if(isdigit(c)) { //c 부분이 숫자면
        int num = (int)c - (int)'0';  //문자로 입력된 것들 숫자로 바꾸는 작업.
        if(number < 0) number = num; 
        else number = number * 10 + num;  //12 나 123 처럼 한자리 이상으로 넘어갈때를 작업 해주는 과정.
      }
      else if(c!=' ') name+=c;
    }
    timezone_[name]=number;       //도시와 시간차이를 저장
  }
}
WorldClock::WorldClock(int hour, int minute, int second) {
  all_count=3600*hour+60*minute+second;
}

void WorldClock::Tick(int seconds) {
  all_count+=seconds;
}
// 잘못된 값 입력시 false 리턴하고 원래 시간은 바뀌지 않음.
bool WorldClock::SetTime(int hour, int minute, int second) {
  if(hour>=60||minute>=60||second>=60) return false;
  else {
    all_count=3600*hour+60*minute+second;
    return true;
  }
}

bool WorldClock::LoadTimezoneFromFile(const string& file_path) {  //파일에서 찾아온 내용들에서 해당하는 도시의 시차(?)를 저장하는 함수
  int check=0;
  //map<string,int>::iterator it;
  for(it=timezone_.begin();it!=timezone_.end();it++) {
    if(it->first==file_path) {
      time_difference_=(it->second);
      check=1;
    }
  }
  if(check==0) return false;
  else return true;
}
void WorldClock::SaveTimezoneToFile() {
  ofstream myfile;
  myfile.open ("timezone.txt");
  for(it=timezone_.begin();it!=timezone_.end();it++) {
    string put_letter;  //파일 안에 넣을 string

    //숫자 diff를 string으로 바꿈
    stringstream ss;
    ss<<it->second;
    string index=ss.str();
  
    //put_letter을 파일 형식에 맞게 셋팅
    put_letter+=it->first;
    put_letter+=" ";
    put_letter+=index;  

    //파일에 작성
    myfile << put_letter <<endl;
  }
  myfile.close();
}

void WorldClock::AddTimezoneInfo(const string& city, int diff) {
  timezone_[city]=diff;
}

// 잘못된 값 입력시 false 리턴하고 원래 시간은 바뀌지 않음.
bool WorldClock::SetTimezone(const string& timezone) {
  time_difference_=timezone_[timezone];
}

int WorldClock::hour() const {
  if(all_count>=0) {
    if((all_count/3600)+time_difference()>24) return all_count/3600-24;
    else return all_count/3600;
  }
  else {
    return (24-((-1)*all_count)/3600);
  }
}
int WorldClock::minute() const {
  if(all_count>=0) return (all_count%3600)/60; 
  else {
    return (60-((-1)*all_count)%3600/60);
  }
}
int WorldClock::second() const {
  if(all_count>=0)  return (all_count%3600)%60; 
  else {
    return (60-((-1)*all_count)%3600/60);
  }
}



// hh:mm:ss 형식으로 입출력. 표준시가 아닌 경우 (+xx)/(-xx) 형식으로 시차를 표시.
ostream& operator<<(ostream& os, const WorldClock& c) {
  if(c.time_difference()==0) os<<c.hour()+c.time_difference()<<":"<<c.minute()<<":"<<c.second();
  else os<<c.hour()+c.time_difference()<<":"<<c.minute()<<":"<<c.second()<<' '<<'('<<'+'<<c.time_difference()<<')';
  return os;
}

// hh:mm:ss 로 입력받음.
// 사용자 입력 오류시 >> operator는 InvalidDateException을 발생할 수 있음.
istream& operator>>(istream& is, WorldClock& c) {
  string input_time;
  vector<int> hms;
  int number=-1;
  cin>>input_time;
  InvalidTimeException Invalid(input_time);

  for(int i=0;i<input_time.length();++i) {
    char a=input_time[i];
    if(isdigit(a)) { //a 부분이 숫자면
      int num = (int)a - (int)'0';  //문자로 입력된 것들 숫자로 바꾸는 작업.
      if(number < 0) number = num; 
      else number = number * 10 + num;  //12 나 123 처럼 한자리 이상으로 넘어갈때를 작업 해주는 과정.
    }
    else if(a==':') {
      hms.push_back(number);
      number=-1;
    }
  }
  hms.push_back(number);
  //시 분 초 가 순서대로 hms벡터에 저장됨 
  int hour=hms[0];
  int min=hms[1];
  int sec=hms[2];
  hms.clear();
  if(c.SetTime(hour, min, sec)==false) {
    throw Invalid;
  }
  return is;
}
