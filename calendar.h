#ifndef _CALENDAR_H_
#define _CALENDAR_H_

#include <iostream>
#include <vector>


class Date {
 public:
  Date();
  Date(int year, int month, int day);

  void NextDay(int n = 1);
  bool SetDate(int year, int month, int day);

  int year() const;
  int month() const;
  int day() const;

 private:
  // 윤년을 판단하여 주어진 연도에 해당하는 날짜 수(365 또는 366)를 리턴.
  int GetDaysInYear(int year) {    //4로 나누어지는 년이 윤년. 그중에서 100으로 나누어떨어지는 년은 평년. 다시 400으로 나누어 떨어지는 년은 윤년
    if(year%4==0) {
      if(year%100==0) {
        if(year%400==0) return 366;  //윤년
        else return 365;  //평년
      }
      else return 366;  //윤년
    }
    else return 365;  //평년
  }
      
      
  // 해당 날짜가 해당 연도의 처음(1월 1일)부터 며칠째인지를 계산.
  int ComputeDaysFromYearStart(int year, int month, int day) {
    int date;
    if(GetDaysInYear(year)==365) {    //평년일때
      for(int i=0;i<month;++i) {
        date+=normal_year[i];
      }
      date+=day;
    }
    else  {  //윤년일때
      for(int i=0;i<month;++i) {
        date+=normal_year[i];
      }
      date+=day;
    }
    return date;
  }

  int year_, month_, day_;
  vector<int> normal_year;  //평년
  vector<int> leap_year;   //윤년
};

struct InvalidDateException {
  string input_date;
  InvalidDateException(const string& str) : input_date(str) {}
};

// yyyy.mm.dd 형식으로 입출력.
// 사용자 입력 오류시 >> operator는 InvalidDateException을 발생할 수 있음.
ostream& operator<<(ostream& os, const Date& c);
istream& operator>>(istream& is, Date& c);

#endif /* calendar.h */
