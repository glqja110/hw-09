#ifndef _POLY_DIFF_H_
#define _POLY_DIFF_H_

#include <iostream>
#include <vector>
#include <string>

using namespace std; 

class Calculus { 
  public:
    Calculus() {};
    ~Calculus() {};
    void examine(string all_letters);
    int Result(int put_number);

  private:
    vector<int> coefficient;  //계수 모아놓는 벡터
    vector<int> upper_number;  //제곱수 모아놓는 벡터
};

#endif /* poly_diff.h */
