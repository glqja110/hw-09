using namespace std;
#include "calendar.h"

int main() {
  Date date; 
  string cmd;
  while (cmd != "quit") {
    cin >> cmd;
    try {
      if (cmd == "set") {
        cin>>date;
        cout<<date<<endl;
      } else if (cmd == "next_day") {
        date.NextDay();
        cout<<date<<endl;
      } else if (cmd == "next") {
        int next=1;
        cin >> next;
        date.NextDay(next);
        cout << date << endl;
      }
    } catch (InvalidDateException& e) {
      cout << "Invalid date: " << e.input_date << endl;
    }
  }
  return 0;
}
