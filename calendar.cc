using namespace std;
#include "calendar.h"

Date::Date() {
  year_=0;
  month_=0;
  day_=0;
  normal_year.reserve(12);
  leap_year.reserve(12);
  for(int i=1;i<=12;++i) {
    if(i==1||i==3||i==5||i==7||i==8||i==10||i==12) {
      normal_year[i-1]=31;
    } else if(i==4||i==6||i==9||i==11) {
      normal_year[i-1]=30;
    } else if(i==2) {
      normal_year[i-1]=28;
    }
  }

  for(int i=1;i<=12;++i) {
    if(i==1||i==3||i==5||i==7||i==8||i==10||i==12) {
      leap_year[i-1]=31;
    } else if(i==4||i==6||i==9||i==11) {
      leap_year[i-1]=30;
    } else if(i==2) {
      leap_year[i-1]=29;
    }
  }
}

Date::Date(int year, int month, int day) {
  year_=year;
  month_=month;
  day_=day;
}

void Date::NextDay(int n) {
  if(n>=0) {   //증가 하는 형태일 때
    if(GetDaysInYear(year_)==365) {  //평년일 때
      if(day_+n>normal_year[month_-1]) {  //날짜를 추가했는데 그 달의 날짜보다 커지면
        if(month_==12) { //해당 날짜가 그 달의 마지막 달인데다가 12월 달이면 
          year_++;
          int temd=day_+n-normal_year[month_-1];
          day_=temd;
          month_=1;
        }
        else {
          int temd=day_+n-normal_year[month_-1];
          day_=temd;
          month_++;
        }
      }
      else {
        day_+=n;
      }
    }
    else {    //윤년일 때
      if(day_+n>leap_year[month_-1]) {  //날짜를 추가했는데 그 달의 날짜보다 커지면
        if(month_==12) { //해당 날짜가 그 달의 마지막 달인데다가 12월 달이면 
          ++year_;
          int temd=day_+n-normal_year[month_-1];
          day_=temd;
          month_=1;
        }
        else {
          int temd=day_+n-normal_year[month_-1];
          day_=temd;
          ++month_;
        }
      }
      else {
        day_+=n;
      }
    }
  }
  else {  //감소하는 형태일 때
    if(GetDaysInYear(year_)==365) {  //평년일 때
      if(day_+n<=0) {  //날짜를 뺐는데 음수가 되면
        if(month_==1) { //1월 달이면 
          --year_;
          int temd=normal_year[11]+(n+day_);
          day_=temd;
          month_=12;
        }
        else {
          int temd=normal_year[month_-2]+(n+day_);
          day_=temd;
          --month_;
        }
      }
      else {
        day_+=n;
      }
    }
    else {    //윤년일 때
      if(day_+n<=0) {  //날짜를 뺐는데 음수가 되면
        if(month_==1) { //1월 달이면 
          --year_;
          int temd=normal_year[11]+(n+day_);
          day_=temd;
          month_=12;
        }
        else {
          int temd=normal_year[month_-2]+(n+day_);
          day_=temd;
          --month_;
        }
      }
      else {
        day_+=n;
      }
    }
  }
} 

bool Date::SetDate(int year, int month, int day) {
  if(year==0&&month==0&&day==0) return false;  //잘못된 입력
  else if(GetDaysInYear(year)==365) {  //평년일때 2월에다 28일 입력하면
    if(month==2&&day==29) return false;  
    else {
      year_=year;
      month_=month;
      day_=day;
      return true;
    }
  }
  else {  //윤년일 때
    year_=year;
    month_=month;
    day_=day;
    return true;
  }
}

int Date::year() const {
  return year_;
}

int Date::month() const {
  return month_;
}

int Date::day() const {
  return day_;
}


ostream& operator<<(ostream& os, const Date& c) {
  os<<c.year()<<'.'<<c.month()<<'.'<<c.day();
}

istream& operator>>(istream& is, Date& c) {
  string input_cal;
  vector<int> YMD;  //연월일 순서대로 저장하는 벡터
  int number=-1;
  is>> input_cal;

  InvalidDateException Invalid(input_cal);

  for(int i=0;i<input_cal.length();++i) {
    char a=input_cal[i];
    if(isdigit(a)) { //a 부분이 숫자면
      int num = (int)a - (int)'0';  //문자로 입력된 것들 숫자로 바꾸는 작업.
      if(number < 0) number = num; 
      else number = number * 10 + num;  //12 나 123 처럼 한자리 이상으로 넘어갈때를 작업 해주는 과정.  
    }
    else if(a=='.') {
      YMD.push_back(number);
      number=-1;
    }
  }
  YMD.push_back(number);
  //연 월 일 이 순서대로 YMD 벡터에 저장됨  
  int YEAR=YMD[0];
  int MON=YMD[1];
  int DATE=YMD[2];
  YMD.clear();
  if(c.SetDate(YEAR, MON, DATE)==false) {
    throw Invalid;
  }
  return is;
}
